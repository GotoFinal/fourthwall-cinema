# To run

Requires mongoDb and api key to OMDb.  
Service can be launched in memory (without mongodb) using "inmem" profile.  

Launch with custom pass and key:
`./gradlew bootRun --args='--adminPassword=pass --omdb.apiKey=key'`  
Launch with custom pass and kay from profile: 
`./gradlew bootRun --args='--spring.profiles.active=prod'`  
Launch without mongodb:
`./gradlew bootRun --args='--adminPassword=pass --omdb.apiKey=key --spring.profiles.active=inmem'`  


## Notes about my solution:
- Many notes are included as TODO comments, so stuff I would add if more time would be given or if it would be needed
- If this would be made in project with some task board, issue list or anything like that then each commit would include some reference to related task.
- Larger modules would have some classes separated from facade, like MovieCreator or MovieRemover, moving such code is easy and no reason to add too much abstraction.
- Endpoints, repositories, and clients should have some metrics 
- Tests could be improved by creating separate gradle module for integration tests
- Its small cinema so I assume they dont need any support for currency
- I also assume owner can control the showtimes manually, and might set different prices for same movies depending on time.
- Would be nice to add user accounts too, so reviews cant be spammed so easily etc.

Sadly there is no OpenApi/swagger, I'm not yet familiar with that, 
and didn't want to just drop few dependencies and jsons hoping it will kinda do what it should,
without first reading more and learning it in some better way - as I didn't have enough time during this task for this.  
