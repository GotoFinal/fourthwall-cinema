package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.rating.RatingFacade
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import groovy.transform.CompileStatic
import org.springframework.data.domain.Pageable

import static com.gotofinal.fourthwall.cinema.rating.base.SampleCreateRatingRequest.sampleCreateRatingRequest

@CompileStatic
trait UsesRatingFacade {
    abstract RatingFacade getRatingFacade()

    RatingDto createRating(Map<String, ?> params) {
        return ratingFacade.create(sampleCreateRatingRequest(params)).block()
    }

    SearchResult<RatingDto> getLastReviews(String movieId, Pageable pageable = Pageable.unpaged()) {
        return ratingFacade.getLastReviews(movieId, pageable).block()
    }
}