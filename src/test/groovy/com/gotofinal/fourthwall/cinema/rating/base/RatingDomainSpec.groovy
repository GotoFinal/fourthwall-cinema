package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.SpecMovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.UsesMovieFacade
import com.gotofinal.fourthwall.cinema.rating.RatingFacade
import spock.lang.Specification

class RatingDomainSpec extends Specification implements UsesRatingFacade, UsesMovieFacade {
    MovieFacade movieFacade = SpecMovieFacade.movieFacade()
    RatingFacade ratingFacade = SpecRatingFacade.ratingFacade(movieFacade: movieFacade)
}
