package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.rating.base.RatingDomainSpec
import com.gotofinal.fourthwall.cinema.shared.validation.ValidationException
import org.junit.Before
import spock.lang.Unroll

class RatingSpec extends RatingDomainSpec {
    MovieDto movie

    @Before
    void prepareMovie() {
        movie = createMovie("title", "id")
    }

    def "should create rating"() {
        given: "rating user, value and review"
            String user = "My user"
            int value = 4
            String review = "Some review"
        when: "you try to create a rating"
            RatingDto ratingDto = createRating(user: user, rating: value, review: review, movieId: movie.id)
        then: "valid rating is created"
            ratingDto.user == user
            ratingDto.rating == value
            ratingDto.review == review
            ratingDto.movieId == movie.id
    }

    @Unroll
    def "should fail to create invalid ratings #testCase"() {
        when: "you try to create a rating with invalid property"
            createRating([movieId: movie.id] + props)
        then: "error is thrown"
            thrown(ValidationException)
        where:
            testCase                     | props
            "with too long user name"    | [user: "x" * 300]
            "with too long review"       | [review: "x" * 3000]
            "with rating smaller than 1" | [rating: 0]
            "with rating larger than 5"  | [rating: 6]
            "with non existing movie"    | [movieId: "nope"]
    }
}
