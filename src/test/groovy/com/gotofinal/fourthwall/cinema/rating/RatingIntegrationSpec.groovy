package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.rating.base.RatingIntegrationBaseSpec
import org.junit.Before
import org.springframework.test.web.reactive.server.WebTestClient
import spock.lang.Unroll

class RatingIntegrationSpec extends RatingIntegrationBaseSpec {
    MovieDto movie

    @Before
    void prepareMovie() {
        movie = createMovie("title", "id")
    }

    def "should create rating"() {
        given: "rating user, value and review"
            String user = "My user"
            int value = 4
            String review = "Some review"
        when: "you try to create a rating"
            WebTestClient.ResponseSpec response = ratingClient.createRating(user: user, rating: value, review: review, movieId: movie.id)
        then: "valid rating is created"
            response.expectStatus().is2xxSuccessful()
            response.expectBody()
                .jsonPath("user").isEqualTo(user)
                .jsonPath("rating").isEqualTo(value)
                .jsonPath("movieId").isEqualTo(movie.id)
                .jsonPath("review").isEqualTo(review)
    }

    @Unroll
    def "should fail to create invalid ratings #testCase"() {
        when: "you try to create a rating with invalid property"
            WebTestClient.ResponseSpec response = ratingClient.createRating([movieId: movie.id] + props)
        then: "error response is returned"
            response.expectStatus()
                .is4xxClientError()
            response.expectBody()
                .jsonPath("message").isEqualTo(errorMessage)
        where:
            testCase                     | props             | errorMessage
            "with rating smaller than 1" | [rating: 0]       | "Rating must be between 1 and 5, but got: 0"
            "with non existing movie"    | [movieId: "nope"] | "Can't rate not existing movie nope"
    }
}
