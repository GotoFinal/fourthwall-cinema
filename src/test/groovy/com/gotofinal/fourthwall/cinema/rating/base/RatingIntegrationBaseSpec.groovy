package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.base.MvcIntegrationSpec
import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.UsesMovieFacade
import com.gotofinal.fourthwall.cinema.rating.RatingFacade
import org.springframework.beans.factory.annotation.Autowired

class RatingIntegrationBaseSpec extends MvcIntegrationSpec implements UsesMovieFacade, UsesRatingFacade {
    @Autowired
    MovieFacade movieFacade
    @Autowired
    RatingFacade ratingFacade
}
