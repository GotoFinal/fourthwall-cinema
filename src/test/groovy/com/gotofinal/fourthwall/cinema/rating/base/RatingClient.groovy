package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.rating.api.CreateRatingRequest
import org.springframework.data.domain.Pageable
import org.springframework.test.web.reactive.server.WebTestClient

class RatingClient {
    private final WebTestClient webClient

    RatingClient(WebTestClient webClient) {
        this.webClient = webClient
    }

    WebTestClient.ResponseSpec createRating(Map<String, ?> request) {
        return webClient.post()
            .uri("/ratings")
            .bodyValue(SampleCreateRatingRequest.sampleCreateRatingRequest(request))
            .exchange()
    }

    WebTestClient.ResponseSpec getLastReviews(String movieId, Pageable pageable = Pageable.unpaged()) {
        if (pageable.paged) {
            return webClient.get()
                .uri("/ratings/movie/{movieId}?size={size}&page={page}",
                    [
                        movieId: movieId,
                        size   : pageable.pageSize,
                        page   : pageable.pageNumber
                    ]
                )
                .exchange()
        }
        return webClient.get().uri("/ratings/movie/{movieId}", [movieId: movieId]).exchange()
    }

}
