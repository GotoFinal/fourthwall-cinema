package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.SpecMovieFacade
import com.gotofinal.fourthwall.cinema.rating.RatingFacade
import com.gotofinal.fourthwall.cinema.rating.RatingRepository
import com.gotofinal.fourthwall.cinema.rating.infrastructure.InMemoryRatingRepository
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import groovy.transform.CompileStatic

import java.time.Clock

import static com.gotofinal.fourthwall.cinema.base.IncrementalIdGenerator.incrementalIdGenerator

@CompileStatic
class SpecRatingFacade {
    static Map<String, ?> defaultDependencies() {
        return [
            clock      : Clock.systemUTC(),
            movieFacade: SpecMovieFacade.movieFacade(),
            repository : new InMemoryRatingRepository(),
            idGenerator: incrementalIdGenerator("rating"),
        ]
    }

    static RatingFacade ratingFacade(Map<String, ?> custom = [:]) {
        Map<String, ?> props = defaultDependencies() + custom
        return new RatingFacade(
            props.movieFacade as MovieFacade,
            props.repository as RatingRepository,
            props.idGenerator as IdGenerator,
            props.clock as Clock,
        )
    }
}
