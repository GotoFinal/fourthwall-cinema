package com.gotofinal.fourthwall.cinema.rating.base

import com.gotofinal.fourthwall.cinema.rating.api.CreateRatingRequest

class SampleCreateRatingRequest {
    static Map<String, ?> defaultProperties() {
        return [
            user   : "Some user name",
            movieId: "m1",
            rating : 3,
            review : "Movie was kinda ok-ish"
        ]
    }

    static CreateRatingRequest sampleCreateRatingRequest(Map<String, ?> customProperties = [:]) {
        Map<String, ?> properties = defaultProperties() + customProperties
        return new CreateRatingRequest(
            properties.user as String,
            properties.movieId as String,
            properties.rating as Integer,
            properties.review as String
        )
    }
}
