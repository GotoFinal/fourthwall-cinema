package com.gotofinal.fourthwall.cinema.movie.base

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.api.CreateMovieRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import groovy.transform.CompileStatic
import org.springframework.data.domain.Pageable

@CompileStatic
trait UsesMovieFacade {
    abstract MovieFacade getMovieFacade()

    MovieDto createMovie(String title, String imdbId) {
        return movieFacade.create(new CreateMovieRequest(title, imdbId)).block()
    }

    MovieDto getMovie(String id) {
        movieFacade.getMovie(id).block()
    }

    void deleteMovie(String id) {
        movieFacade.delete(id).block()
    }

    SearchResult<MovieDto> searchMovies(String phrase = null, Pageable pageable = Pageable.unpaged()) {
        return movieFacade.search(new GetMoviesRequest(phrase, pageable)).block()
    }
}