package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.base.MovieDomainSpec
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.PageRequest

// TODO: separate to other classes like CreateMovieSpec if too large
class MovieSpec extends MovieDomainSpec {
    def "should create movie"() {
        given: "movie title and imdbId"
            String title = "The Fast and the Furious"
            String imdbId = "tt0232500"
        when: "you try to create a movie"
            MovieDto movieDto = createMovie(title, imdbId)
        then: "valid movie is created"
            movieDto.title == title
            movieDto.imdbId == imdbId
    }

    // TODO: test for failing movie creation if id is invalid

    def "should get movie by id"() {
        given: "a movie"
            String movieId = createMovie("title", "imdbId").id
            assert searchMovies().size == 1
        when: "you try to get a movie by id"
            MovieDto movieDto = getMovie(movieId)
        then: "valid movie is fetched"
            movieDto.title == "title"
    }

    def "should delete movie"() {
        given: "a movie"
            MovieDto movieDto = createMovie("title", "imdbId")
            assert searchMovies().size == 1
        when: "you try to remove a movie"
            deleteMovie(movieDto.id)
        then: "move is removed"
            searchMovies().size == 0
    }

    def "should search for movies with pagination"() {
        given: "a list of movies"
            (1..10).forEach { createMovie("title $it", "a$it") }
            (1..5).forEach { createMovie("otherTitle $it", "b$it") }
            assert searchMovies().size == 15
        when: "you search for 3rd page of movies starting with a with page of 2"
            SearchResult<MovieDto> result = searchMovies("title", PageRequest.of(3, 2))
        then: "you get 2 valid results and total size"
            result.totalElements == 10
            result.size == 2
            result.content.title == ["title 6", "title 7"]
            result.content.imdbId == ["a6", "a7"]
    }

    def "should never feed same movie twice"() {
        given: "2 movies to feed"
            List<DefaultMovie> movies = [new DefaultMovie("title", "1"), new DefaultMovie("title", "2")]
        when: "movies are feed first time"
            long count = movieFacade.feedMovies(movies).block()
        then: "movies are successfully feed"
            count == 2
        when: "movies are feed second time"
            count = movieFacade.feedMovies(movies).block()
        then: "no movies are feed"
            count == 0
    }
}
