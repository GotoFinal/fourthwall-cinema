package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.movie.api.CreateMovieRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.base.MovieIntegrationBaseSpec
import org.springframework.data.domain.PageRequest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.reactive.server.WebTestClient

class MovieIntegrationSpec extends MovieIntegrationBaseSpec {
    def "should never feed same movie twice"() {
        given: "2 movies to feed"
            List<DefaultMovie> movies = [new DefaultMovie("title", "1"), new DefaultMovie("title", "2")]
        when: "movies are feed first time"
            long count = movieFacade.feedMovies(movies).block()
        then: "movies are successfully feed"
            count == 2
        when: "movies are feed second time"
            count = movieFacade.feedMovies(movies).block()
        then: "no movies are feed"
            count == 0
    }

    @WithMockUser(roles = "ADMIN")
    def "should create movie"() {
        given: "movie title and imdbId"
            String title = "The Fast and the Furious"
            String imdbId = "tt0232500"
        when: "you try to create a movie"
            WebTestClient.ResponseSpec response = movieClient.createMovie(new CreateMovieRequest(title, imdbId))
        then: "valid movie is created"
            response.expectStatus().is2xxSuccessful()
            response.expectBody()
                .jsonPath("title").isEqualTo(title)
                .jsonPath("imdbId").isEqualTo(imdbId)
    }

    def "should fail to create movie without admin role"() {
        when: "you try to create a movie"
            WebTestClient.ResponseSpec response = movieClient.createMovie(new CreateMovieRequest("title", "id"))
        then: "you get redirected to login page"
            response.expectStatus().is3xxRedirection()
    }

    def "should get movie by id"() {
        given: "a movie"
            String movieId = createMovie("title", "imdbId").id
            assert searchMovies().size == 1
        when: "you try to get a movie by id"
            MovieDto movieDto = getMovie(movieId)
        then: "valid movie is fetched"
            movieDto.title == "title"
    }

    @WithMockUser(roles = "ADMIN")
    def "should delete movie"() {
        given: "a movie"
            MovieDto movieDto = createMovie("title", "imdbId")
            assert searchMovies().size == 1
        when: "you try to remove a movie"
            WebTestClient.ResponseSpec response = movieClient.deleteMovie(movieDto.id)
        then: "move is removed"
            response.expectStatus().is2xxSuccessful()
            searchMovies().size == 0
    }

    def "should fail to delete movie without admin role"() {
        when: "you try to remove a movie"
            WebTestClient.ResponseSpec response = movieClient.deleteMovie("someId")
        then: "you get redirected to login page"
            response.expectStatus().is3xxRedirection()
    }

    def "should search for movies with pagination"() {
        given: "a list of movies"
            (1..10).forEach { createMovie("title $it", "a$it") }
            (1..5).forEach { createMovie("otherTitle $it", "b$it") }
            assert searchMovies().size == 15
        when: "you search for 3rd page of movies starting with a with page of 2"
            WebTestClient.ResponseSpec response = movieClient.searchMovies(new GetMoviesRequest("title", PageRequest.of(3, 2)))
        then: "you get 2 valid results and total size"
            response.expectStatus().is2xxSuccessful()
            response.expectBody()
                .jsonPath("page").isEqualTo("3")
                .jsonPath("pageSize").isEqualTo("2")
                .jsonPath("content.*.title").isEqualTo(["title 6", "title 7"])
                .jsonPath("content.*.imdbId").isEqualTo(["a6", "a7"])
    }
}
