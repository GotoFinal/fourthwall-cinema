package com.gotofinal.fourthwall.cinema.movie.base

import com.gotofinal.fourthwall.cinema.movie.api.CreateMovieRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import org.springframework.test.web.reactive.server.WebTestClient

class MovieClient {
    private final WebTestClient webClient

    MovieClient(WebTestClient webClient) {
        this.webClient = webClient
    }

    WebTestClient.ResponseSpec getMovieWithMetadata(String id) {
        return webClient.get()
            .uri("/movies/{id}", [id: id])
            .exchange()
    }

    WebTestClient.ResponseSpec createMovie(CreateMovieRequest request) {
        return webClient.post()
            .uri("/movies")
            .bodyValue(request)
            .exchange()
    }

    WebTestClient.ResponseSpec deleteMovie(String id) {
        return webClient.delete()
            .uri("/movies/{id}", [id: id])
            .exchange()
    }

    WebTestClient.ResponseSpec searchMovies(GetMoviesRequest request) {
        if (request.pageable.paged) {
            return webClient.get()
                .uri("/movies?phrase={phrase}&size={size}&page={page}",
                    [
                        phrase: request.phrase,
                        size  : request.pageable.pageSize,
                        page  : request.pageable.pageNumber
                    ]
                )
                .exchange()
        }
        return webClient.get().uri("/movies?phrase={phrase}", [phrase: request.phrase]).exchange()
    }

}
