package com.gotofinal.fourthwall.cinema.movie.base

import com.gotofinal.fourthwall.cinema.base.MvcIntegrationSpec
import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import org.springframework.beans.factory.annotation.Autowired

class MovieIntegrationBaseSpec extends MvcIntegrationSpec implements UsesMovieFacade {
    @Autowired
    MovieFacade movieFacade
}
