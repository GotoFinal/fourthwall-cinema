package com.gotofinal.fourthwall.cinema.movie.infrastructure.omdb


import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.base.MovieIntegrationBaseSpec
import org.springframework.test.web.reactive.server.WebTestClient

class MovieWithMetadataIntegrationSpec extends MovieIntegrationBaseSpec implements OMDbStubs {

    def "should fetch movie with metadata"() {
        given: "2 movies in our and in omdb"
            MovieDto movieA = createMovie("some title", "someId")
            MovieDto movieB = createMovie("some other title", "someOtherId")
            stubOMDbEndpoint(movieA.title, movieA.imdbId)
            stubOMDbEndpoint(movieB.title, movieB.imdbId)
        when: "you try to fetch movie wih metadata"
            WebTestClient.ResponseSpec response = movieClient.getMovieWithMetadata(movieA.id)
        then: "correct response with metadata is returned"
            response.expectStatus().is2xxSuccessful()
            response.expectBody()
                .jsonPath("id").isEqualTo(movieA.id)
                .jsonPath("title").isEqualTo(movieA.title)
                .jsonPath("genre").isEqualTo("Action, Crime, Thriller")
                .jsonPath("description").isEqualTo("Los Angeles police officer Brian O'Conner must decide where his loyalty really lies when he becomes enamored with the street racing world he has been sent undercover to destroy.")
                .jsonPath("image").isEqualTo("https://m.media-amazon.com/images/M/MV5BNzlkNzVjMDMtOTdhZC00MGE1LTkxODctMzFmMjkwZmMxZjFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg")
                .jsonPath("language").isEqualTo("English, Spanish")
                .jsonPath("rating.*.value").isEqualTo(["6.8/10", "53%", "58/100"])
                .jsonPath("runtime").isEqualTo("106 min")
                .jsonPath("release").isEqualTo("22 Jun 2001")
                .jsonPath("metadataProvider").isEqualTo("OMDb")
                .jsonPath("metadataId").isEqualTo(movieA.imdbId)
    }
}
