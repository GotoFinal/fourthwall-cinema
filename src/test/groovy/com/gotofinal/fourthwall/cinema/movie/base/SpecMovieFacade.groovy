package com.gotofinal.fourthwall.cinema.movie.base

import com.gotofinal.fourthwall.cinema.movie.DefaultMovie
import com.gotofinal.fourthwall.cinema.movie.MovieConfigurationProperties
import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.MovieRepository
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.infrastructure.InMemoryMovieRepository
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieMetadataClient
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieWithMetadata
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import groovy.transform.CompileStatic
import reactor.core.publisher.Mono

import static com.gotofinal.fourthwall.cinema.base.IncrementalIdGenerator.incrementalIdGenerator

@CompileStatic
class SpecMovieFacade {
    static Map<String, ?> defaultDependencies() {
        return [
            repository : new InMemoryMovieRepository(),
            idGenerator: incrementalIdGenerator("movie"),
            properties : new MovieConfigurationProperties(new ArrayList<DefaultMovie>()),
            movieMetadataClient : new MovieMetadataClient() {
                @Override
                Mono<MovieWithMetadata> fetchMetadata(MovieDto movieDto) {
                    return Mono.empty()
                }
            }
        ]
    }

    static MovieFacade movieFacade(Map<String, ?> custom = [:]) {
        Map<String, ?> props = defaultDependencies() + custom
        return new MovieFacade(
            props.repository as MovieRepository,
            props.idGenerator as IdGenerator,
            props.properties as MovieConfigurationProperties,
            props.movieMetadataClient as MovieMetadataClient
        )
    }
}
