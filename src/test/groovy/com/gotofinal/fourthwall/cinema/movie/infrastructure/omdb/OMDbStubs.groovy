package com.gotofinal.fourthwall.cinema.movie.infrastructure.omdb

import com.github.tomakehurst.wiremock.WireMockServer
import com.gotofinal.fourthwall.cinema.base.IntegrationSpec
import groovy.transform.CompileStatic
import groovy.transform.SelfType

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo
import static com.github.tomakehurst.wiremock.client.WireMock.get
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo
import static com.gotofinal.fourthwall.cinema.base.WireMockTestConfig.PREFIX_FOR_OMDB_ENDPOINTS
import static org.springframework.http.HttpHeaders.ACCEPT
import static org.springframework.http.HttpHeaders.CONTENT_TYPE
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE

@CompileStatic
@SelfType(IntegrationSpec)
trait OMDbStubs {
    abstract WireMockServer getWireMockServer()

    void stubOMDbEndpoint(
        String title = "The Fast and the Furious",
        String imdbID = "tt0232500",
        int status = 200
    ) {
        wireMockServer.stubFor(
            get(urlEqualTo("$PREFIX_FOR_OMDB_ENDPOINTS?apikey=key&i=$imdbID"))
                .withHeader(ACCEPT, equalTo(APPLICATION_JSON_VALUE))
                .willReturn(
                    aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withBody(SampleOMDbResponse.sampleOMDbResponse(title, imdbID))
                        .withStatus(status)
                )
        )
    }
}