package com.gotofinal.fourthwall.cinema.base

import com.github.tomakehurst.wiremock.WireMockServer
import com.gotofinal.fourthwall.cinema.CinemaApplication
import com.gotofinal.fourthwall.cinema.infrastructure.Profiles
import org.junit.After
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

@ActiveProfiles(Profiles.INTEGRATION)
@ContextConfiguration(initializers = [WireMockTestConfig])
@SpringBootTest(classes = [CinemaApplication], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class IntegrationSpec extends Specification implements DatabaseCleaner {
    @Autowired
    ReactiveMongoOperations mongo
    @Autowired
    WireMockServer wireMockServer

    @After
    void cleanUpDependencies() {
        dropAllCollections()
        wireMockServer.resetAll()
    }
}