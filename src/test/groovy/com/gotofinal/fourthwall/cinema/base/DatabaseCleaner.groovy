package com.gotofinal.fourthwall.cinema.base

import com.mongodb.BasicDBObject
import groovy.transform.CompileStatic
import groovy.transform.SelfType

@CompileStatic
@SelfType(IntegrationSpec)
trait DatabaseCleaner {
    void dropAllCollections() {
        mongo.getCollectionNames()
                .flatMap { mongo.getCollection(it) }
                .flatMap { it.deleteMany(new BasicDBObject()) }
                .blockLast()
    }
}