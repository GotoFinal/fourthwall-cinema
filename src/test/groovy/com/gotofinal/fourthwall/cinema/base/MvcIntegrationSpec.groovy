package com.gotofinal.fourthwall.cinema.base

import com.gotofinal.fourthwall.cinema.movie.base.MovieClient
import com.gotofinal.fourthwall.cinema.rating.base.RatingClient
import com.gotofinal.fourthwall.cinema.showtime.base.ShowtimeClient
import groovy.transform.CompileStatic
import org.junit.Before
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.reactive.server.WebTestClient

@CompileStatic
class MvcIntegrationSpec extends IntegrationSpec {
    @Autowired
    WebTestClient webTestClient
    MovieClient movieClient
    RatingClient ratingClient
    ShowtimeClient showtimeClient

    @Before
    void setupClients() {
        movieClient = new MovieClient(webTestClient)
        ratingClient = new RatingClient(webTestClient)
        showtimeClient = new ShowtimeClient(webTestClient)
    }
}
