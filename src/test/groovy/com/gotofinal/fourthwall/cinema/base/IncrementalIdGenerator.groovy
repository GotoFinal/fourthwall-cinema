package com.gotofinal.fourthwall.cinema.base

import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import com.gotofinal.fourthwall.cinema.shared.id.IdGeneratorFactory
import groovy.transform.CompileStatic
import org.jetbrains.annotations.NotNull

import java.util.concurrent.atomic.AtomicLong

@CompileStatic
class IncrementalIdGenerator implements IdGenerator {
    private final String prefix
    private final AtomicLong counter = new AtomicLong()

    private IncrementalIdGenerator(String prefix) {
        this.prefix = Objects.requireNonNull(prefix) + "::"
    }

    @Override
    String newId() {
        return prefix + counter.incrementAndGet()
    }

    static IdGeneratorFactory incrementalIdGeneratorFactory() {
        return new IdGeneratorFactory() {
            @Override
            IdGenerator create(@NotNull String seed) {
                return incrementalIdGenerator(seed)
            }
        }
    }

    static IdGenerator incrementalIdGenerator(String prefix = "") {
        return new IncrementalIdGenerator(prefix)
    }
}