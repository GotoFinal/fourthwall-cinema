package com.gotofinal.fourthwall.cinema.base

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import groovy.transform.CompileStatic
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.MapPropertySource

@CompileStatic
@Configuration
class WireMockTestConfig implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final int WIRE_MOCK_PORT = nextPort()
    public static final String PREFIX_FOR_OMDB_ENDPOINTS = "/omdb"

    private final Map overrides = [
        "omdb.uri": createBaseUri(PREFIX_FOR_OMDB_ENDPOINTS)
    ]

    @Override
    void initialize(ConfigurableApplicationContext configurableApplicationContext) {
        configurableApplicationContext.getEnvironment()
            .getPropertySources()
            .addFirst(new MapPropertySource("wireMockTestSource", overrides))
    }

    @Bean(destroyMethod = "stop")
    WireMockServer wireMockServer() {
        WireMockConfiguration config = WireMockConfiguration.wireMockConfig()
            .port(WIRE_MOCK_PORT)
        WireMockServer wireMockServer = new WireMockServer(config)
        wireMockServer.start()
        return wireMockServer
    }

    private String createBaseUri(String endpointPrefix) {
        return "http://localhost:${WIRE_MOCK_PORT}${endpointPrefix}"
    }

    private static int nextPort() {
        ServerSocket socket = null
        try {
            socket = new ServerSocket(0)
            return socket.getLocalPort();
        } catch (IOException exception) {
            throw new IllegalStateException("No free port?", exception);
        } finally {
            if (socket != null) socket.close()
        }
    }
}