package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.showtime.api.CreateShowtimeRequest

import java.time.Instant

class SampleCreateShowtimeRequest {
    static Map<String, ?> defaultProperties() {
        return [
            startTime: Instant.parse("2020-11-30T18:35:00.00Z"),
            endTime  : Instant.parse("2020-11-30T20:00:00.00Z"),
            movieId  : "m1",
            price    : new BigDecimal("45.5")
        ]
    }

    static CreateShowtimeRequest sampleCreateShowtimeRequest(Map<String, ?> customProperties = [:]) {
        Map<String, ?> properties = defaultProperties() + customProperties
        return new CreateShowtimeRequest(
            properties.startTime as Instant,
            properties.endTime as Instant,
            properties.movieId as String,
            properties.price as BigDecimal
        )
    }
}
