package com.gotofinal.fourthwall.cinema.showtime

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import com.gotofinal.fourthwall.cinema.showtime.base.ShowtimeDomainSpec
import org.junit.Before

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId

class ShowtimeSpec extends ShowtimeDomainSpec {
    MovieDto movie
    MovieDto movieB

    @Before
    void prepareMovie() {
        movie = createMovie("title", "id")
        movieB = createMovie("other title", "id2")
    }

    def "should create showtime"() {
        given: "showtime start and end time, price, and movie"
            Instant startTime = Instant.parse("2020-10-30T18:35:00.00Z")
            Instant endTime = Instant.parse("2020-10-30T20:00:00.00Z")
            BigDecimal price = new BigDecimal("65.65")
        when: "you try to create a showtime"
            ShowtimeDto showtimeDto = createShowtime(startTime: startTime, endTime: endTime, price: price, movieId: movie.id)
        then: "valid showtime is created"
            showtimeDto.startTime == startTime
            showtimeDto.endTime == endTime
            showtimeDto.price == price
            showtimeDto.movieId == movie.id
    }

    def "should delete showtime"() {
        given: "a showtime"
            ShowtimeDto showtimeDto = createShowtime(movieId: movie.id)
            LocalDate showDate = LocalDate.ofInstant(showtimeDto.startTime, ZoneId.of("UTC"))
            GetShowtimesRequest showtimesRequest = new GetShowtimesRequest(showtimeDto.movieId, showDate, showDate)
            assert getShowtimes(showtimesRequest).size() == 1
        when: "you try to remove a showtime"
            deleteShowtime(showtimeDto.id)
        then: "move is removed"
            getShowtimes(showtimesRequest).size() == 0
    }

    def "should list showcases between given dates"() {
        given: "list of showcases at various times"
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-30T18:35:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-30T00:00:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-11-04T23:59:59.99Z"))
            createShowtime(movieId: movieB.id, startTime: Instant.parse("2020-10-30T18:35:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-29T23:59:59.99Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-11-05T00:00:00.00Z"))

        when: "you fetch list of showcases between 10-30 and 11-04"
            List<ShowtimeDto> result = getShowtimes(new GetShowtimesRequest(null, LocalDate.of(2020, 10, 30), LocalDate.of(2020, 11, 04)))
        then: "resulting list contains 4 matching showtimes"
            result.size() == 4
        when: "you fetch showtimes filtered by movie"
            result = getShowtimes(new GetShowtimesRequest(movie.id, LocalDate.of(2020, 10, 30), LocalDate.of(2020, 11, 04)))
        then: "resulting list contains 4 matching showtimes"
            result.size() == 3
    }
}
