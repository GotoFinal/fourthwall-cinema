package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.base.MvcIntegrationSpec
import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.UsesMovieFacade
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeFacade
import org.springframework.beans.factory.annotation.Autowired

class ShowtimeIntegrationBaseSpec extends MvcIntegrationSpec implements UsesMovieFacade, UsesShowtimeFacade {
    @Autowired
    MovieFacade movieFacade
    @Autowired
    ShowtimeFacade showtimeFacade
}
