package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.SpecMovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.UsesMovieFacade
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeFacade
import spock.lang.Specification

class ShowtimeDomainSpec extends Specification implements UsesShowtimeFacade, UsesMovieFacade {
    MovieFacade movieFacade = SpecMovieFacade.movieFacade()
    ShowtimeFacade showtimeFacade = SpecShowtimeFacade.showtimeFacade(movieFacade: movieFacade)
}
