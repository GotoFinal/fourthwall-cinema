package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.showtime.ShowtimeFacade
import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import groovy.transform.CompileStatic

import static com.gotofinal.fourthwall.cinema.showtime.base.SampleCreateShowtimeRequest.sampleCreateShowtimeRequest

@CompileStatic
trait UsesShowtimeFacade {
    abstract ShowtimeFacade getShowtimeFacade()

    ShowtimeDto createShowtime(Map<String, ?> params = [:]) {
        return showtimeFacade.create(sampleCreateShowtimeRequest(params)).block()
    }

    void deleteShowtime(String id) {
        showtimeFacade.delete(id).block()
    }

    List<ShowtimeDto> getShowtimes(GetShowtimesRequest request) {
        return showtimeFacade.getShowtimes(request).collectList().block()
    }
}