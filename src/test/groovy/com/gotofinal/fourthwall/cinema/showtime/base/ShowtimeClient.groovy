package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.base.SampleCreateShowtimeRequest
import org.springframework.test.web.reactive.server.WebTestClient

class ShowtimeClient {
    private final WebTestClient webClient

    ShowtimeClient(WebTestClient webClient) {
        this.webClient = webClient
    }

    WebTestClient.ResponseSpec createShowtime(Map<String, ?> request = [:]) {
        return webClient.post()
            .uri("/showtimes")
            .bodyValue(SampleCreateShowtimeRequest.sampleCreateShowtimeRequest(request))
            .exchange()
    }

    WebTestClient.ResponseSpec deleteShowtime(String id) {
        return webClient.delete()
            .uri("/showtimes/{id}", [id: id])
            .exchange()
    }

    WebTestClient.ResponseSpec getShowtimes(GetShowtimesRequest request) {
        String uri = "/showtimes?fromDate={fromDate}&toDate={toDate}"
        if (request.movieId != null) {
            uri += "&movieId={movieId}"
        }
        return webClient.get()
            .uri(uri,
                [
                    movieId : request.movieId,
                    fromDate: request.fromDate,
                    toDate  : request.toDate
                ]
            )
            .exchange()
    }
}
