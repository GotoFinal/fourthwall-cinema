package com.gotofinal.fourthwall.cinema.showtime

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import com.gotofinal.fourthwall.cinema.showtime.base.ShowtimeIntegrationBaseSpec
import org.junit.Before
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.reactive.server.WebTestClient

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId

class ShowtimeIntegrationSpec extends ShowtimeIntegrationBaseSpec {
    MovieDto movie
    MovieDto movieB

    @Before
    void prepareMovie() {
        movie = createMovie("title", "id")
        movieB = createMovie("other title", "id2")
    }

    @WithMockUser(roles = "ADMIN")
    def "should create showtime"() {
        given: "showtime start and end time, price, and movie"
            Instant startTime = Instant.parse("2020-10-30T18:35:00.00Z")
            Instant endTime = Instant.parse("2020-10-30T20:00:00.00Z")
            BigDecimal price = new BigDecimal("65.65")
        when: "you try to create a showtime"
            WebTestClient.ResponseSpec response = showtimeClient.createShowtime(startTime: startTime, endTime: endTime, price: price, movieId: movie.id)
        then: "valid showtime is created"
            response.expectStatus().is2xxSuccessful()
            response.expectBody()
                .jsonPath("startTime").isEqualTo(startTime.epochSecond)
                .jsonPath("endTime").isEqualTo(endTime.epochSecond)
                .jsonPath("price").isEqualTo(price)
                .jsonPath("movieId").isEqualTo(movie.id)
    }

    def "should fail to create showtime without admin role"() {
        when: "you try to create a showtime"
            WebTestClient.ResponseSpec response = showtimeClient.createShowtime()
        then: "you get redirected to login page"
            response.expectStatus().is3xxRedirection()
    }

    @WithMockUser(roles = "ADMIN")
    def "should delete showtime"() {
        given: "a showtime"
            ShowtimeDto showtimeDto = createShowtime(movieId: movie.id)
            LocalDate showDate = LocalDate.ofInstant(showtimeDto.startTime, ZoneId.of("UTC"))
            GetShowtimesRequest showtimesRequest = new GetShowtimesRequest(showtimeDto.movieId, showDate, showDate)
            assert getShowtimes(showtimesRequest).size() == 1
        when: "you try to remove a showtime"
            WebTestClient.ResponseSpec response = showtimeClient.deleteShowtime(showtimeDto.id)
        then: "move is removed"
            response.expectStatus().is2xxSuccessful()
            getShowtimes(showtimesRequest).size() == 0
    }

    def "should fail to delete showtime without admin role"() {
        when: "you try to remove a showtime"
            WebTestClient.ResponseSpec response = showtimeClient.deleteShowtime("someId")
        then: "you get redirected to login page"
            response.expectStatus().is3xxRedirection()
    }

    def "should list showcases between given dates"() {
        given: "list of showcases at various times"
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-30T18:35:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-30T00:00:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-11-04T23:59:59.99Z"))
            createShowtime(movieId: movieB.id, startTime: Instant.parse("2020-10-30T18:35:00.00Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-10-29T23:59:59.99Z"))
            createShowtime(movieId: movie.id, startTime: Instant.parse("2020-11-05T00:00:00.00Z"))

        when: "you fetch list of showcases between 10-30 and 11-04"
            WebTestClient.ResponseSpec response = showtimeClient.getShowtimes(new GetShowtimesRequest(null, LocalDate.of(2020, 10, 30), LocalDate.of(2020, 11, 04)))
        then: "resulting list contains 4 matching showtimes"
            response.expectStatus().is2xxSuccessful()
            response.expectBodyList(Object.class).hasSize(4)
        when: "you fetch showtimes filtered by movie"
            response = showtimeClient.getShowtimes(new GetShowtimesRequest(movie.id, LocalDate.of(2020, 10, 30), LocalDate.of(2020, 11, 04)))
        then: "resulting list contains 4 matching showtimes"
            response.expectStatus().is2xxSuccessful()
            response.expectBodyList(Object.class).hasSize(3)
    }
}
