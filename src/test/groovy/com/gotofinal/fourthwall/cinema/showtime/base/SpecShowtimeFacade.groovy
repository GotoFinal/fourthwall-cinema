package com.gotofinal.fourthwall.cinema.showtime.base

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.base.SpecMovieFacade
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeFacade
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeRepository
import com.gotofinal.fourthwall.cinema.showtime.infrastructure.InMemoryShowtimeRepository
import groovy.transform.CompileStatic

import java.time.Clock

import static com.gotofinal.fourthwall.cinema.base.IncrementalIdGenerator.incrementalIdGenerator

@CompileStatic
class SpecShowtimeFacade {
    static Map<String, ?> defaultDependencies() {
        return [
            clock      : Clock.systemUTC(),
            movieFacade: SpecMovieFacade.movieFacade(),
            repository : new InMemoryShowtimeRepository(),
            idGenerator: incrementalIdGenerator("showtime"),
        ]
    }

    static ShowtimeFacade showtimeFacade(Map<String, ?> custom = [:]) {
        Map<String, ?> props = defaultDependencies() + custom
        return new ShowtimeFacade(
            props.movieFacade as MovieFacade,
            props.idGenerator as IdGenerator,
            props.repository as ShowtimeRepository,
            props.clock as Clock,
        )
    }
}
