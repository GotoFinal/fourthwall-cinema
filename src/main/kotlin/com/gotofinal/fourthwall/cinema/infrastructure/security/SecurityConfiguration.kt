@file:Suppress("DEPRECATION")

package com.gotofinal.fourthwall.cinema.infrastructure.security

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.core.userdetails.MapReactiveUserDetailsService
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.crypto.password.NoOpPasswordEncoder
import org.springframework.security.web.server.SecurityWebFilterChain

@Configuration
@EnableWebFluxSecurity
class SecurityConfiguration {
    @Bean
    fun securityWebFilterChain(
        http: ServerHttpSecurity
    ): SecurityWebFilterChain? {
        return http.authorizeExchange()
            .anyExchange().permitAll()
            .and().formLogin()
            .and().csrf().disable().build()
    }

    @Bean
    fun userDetailsService(
        @Value("\${adminUsername}") username: String,
        @Value("\${adminPassword}") password: String?
    ): MapReactiveUserDetailsService? {
        if (password.isNullOrBlank()) {
            throw IllegalStateException("Admin account need to have a password")
        }
        val user: UserDetails = User
            .withUsername(username)
            .password(password)
            .roles(ADMIN)
            .build()
        return MapReactiveUserDetailsService(user)
    }

    @Bean
    fun passwordEncoder(): NoOpPasswordEncoder? {
        return NoOpPasswordEncoder.getInstance() as NoOpPasswordEncoder
    }
}

const val ADMIN = "ADMIN"
