package com.gotofinal.fourthwall.cinema.infrastructure

object Profiles {
    const val PROD = "prod"
    const val TEST = "test"
    const val INTEGRATION = "integration"
    const val IN_MEM = "inmem"
    const val NOT_IN_MEM = "!$IN_MEM"
}
