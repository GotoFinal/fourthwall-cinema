package com.gotofinal.fourthwall.cinema.infrastructure.mongo

object MongoCollections {
    const val MOVIES = "movies"
    const val RATINGS = "ratings"
    const val SHOWTIMES = "showtimes"
}
