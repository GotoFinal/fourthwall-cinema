package com.gotofinal.fourthwall.cinema.infrastructure.mongo

import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.Query.of
import org.springframework.data.mongodb.core.query.Query.query

object MongoQueries {
    fun queryById(id: Any): Query {
        return queryBy("_id", id)
    }

    fun queryBy(key: String, value: Any): Query {
        return queryBy(key to value)
    }

    fun queryByIfNotNull(key: String, value: Any?): Query {
        if (value == null) {
            return Query()
        }
        return queryBy(key to value)
    }

    fun queryBy(vararg pairs: Pair<String, Any>): Query {
        val foldedCriteria = pairs.fold(Criteria()) { criteria, pair ->
            criteria.and(pair.first).`is`(pair.second)
        }
        return query(foldedCriteria)
    }
}

fun Query.unpaged(): Query {
    return of(this)
        .limit(0)
        .skip(0)
}
