package com.gotofinal.fourthwall.cinema.movie.metadata.infrastructure.omdb

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieMetadataClient
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieWithMetadata
import com.gotofinal.fourthwall.cinema.shared.logger.logger
import org.slf4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.WebClientResponseException
import org.springframework.web.server.ResponseStatusException
import reactor.core.publisher.Mono
import java.time.Duration

// TODO: this could be probably heavily cached
class OMDbHttpClient(
    private val uri: String,
    private val apiKey: String,
    private val webClient: WebClient,
    private val timeout: Duration
) : MovieMetadataClient {
    private val logger: Logger by logger()

    override fun fetchMetadata(movieDto: MovieDto): Mono<MovieWithMetadata> {
        return webClient.get()
            .uri("$uri?apikey={apiKey}&i={id}", mapOf("apiKey" to apiKey, "id" to movieDto.imdbId))
            .accept(MediaType.APPLICATION_JSON)
            .retrieve()
            .bodyToMono(OMDbMovie::class.java)
            .timeout(timeout)
            .onErrorMap { handleError(it) }
            .map { it.toMovieWithMetadata(movieDto) }
    }

    private fun handleError(throwable: Throwable): Throwable {
        if (throwable is WebClientResponseException) {
            val status = throwable.rawStatusCode
            val body = throwable.responseBodyAsString
            val uri = throwable.request?.uri?.toString()
            val method = throwable.request?.method.toString()
            logger.warn(
                "Error response from http client when trying to fetch movie metadata from OMDb: \n" +
                    "Request: $method $uri\n" +
                    "Status: $status\n" +
                    "Body: $body"
            )
        }
        return ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, "Can't access OMDb api")
    }
}
