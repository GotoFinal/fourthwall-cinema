package com.gotofinal.fourthwall.cinema.movie.metadata.infrastructure.omdb

import com.fasterxml.jackson.annotation.JsonProperty
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieRating
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieWithMetadata

internal data class OMDbMovie(
    @JsonProperty("Title")
    val title: String,
    @JsonProperty("Released")
    val released: String,
    @JsonProperty("Runtime")
    val runtime: String,
    @JsonProperty("Genre")
    val genre: String,
    @JsonProperty("Plot")
    val plot: String,
    @JsonProperty("Language")
    val language: String,
    @JsonProperty("Poster")
    val poster: String,
    @JsonProperty("Ratings")
    val ratings: List<MovieRating>
) {
    fun toMovieWithMetadata(movieDto: MovieDto): MovieWithMetadata {
        return MovieWithMetadata(
            id = movieDto.id,
            title = title,
            genre = genre,
            description = plot,
            image = poster,
            language = language,
            rating = ratings,
            runtime = runtime,
            release = released,
            metadataProvider = "OMDb",
            metadataId = movieDto.imdbId
        )
    }
}
