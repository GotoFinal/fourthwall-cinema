package com.gotofinal.fourthwall.cinema.movie.metadata.infrastructure.omdb

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import java.time.Duration

@ConstructorBinding
@ConfigurationProperties("omdb")
// TODO: properties for cache, better timeouts, etc, maybe circuit breaker 
data class OMDbClientProperties(
    val uri: String,
    val apiKey: String?,
    val maxConnections: Int = 100,
    val timeout: Duration = Duration.ofSeconds(1)
)
