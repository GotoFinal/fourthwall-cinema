package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoCollections
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(MongoCollections.MOVIES)
internal class Movie(
    @Id
    val id: String,
    private val title: String,
    private val imdbId: String
) {
    fun toDto(): MovieDto {
        return MovieDto(
            id = id,
            title = title,
            imdbId = imdbId
        )
    }
}
