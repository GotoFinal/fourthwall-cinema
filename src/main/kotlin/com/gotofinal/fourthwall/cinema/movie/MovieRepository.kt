package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

internal interface MovieRepository {
    fun getById(id: String): Mono<Movie>
    fun getByImdbId(imdbId: List<String>): Flux<Movie>
    fun save(movie: Movie): Mono<Movie>
    fun delete(id: String): Mono<Void>
    fun search(request: GetMoviesRequest): Mono<SearchResult<MovieDto>>
}
