package com.gotofinal.fourthwall.cinema.movie.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.IN_MEM
import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.NOT_IN_MEM
import com.gotofinal.fourthwall.cinema.movie.Movie
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.index.Index
import org.springframework.data.mongodb.core.index.TextIndexDefinition

@Configuration
internal class MovieRepositoryConfiguration {
    @Bean
    @Profile(IN_MEM)
    fun inMemoryMovieRepository(): InMemoryMovieRepository {
        return InMemoryMovieRepository()
    }

    @Bean
    @Profile(NOT_IN_MEM)
    fun mongoMovieRepository(mongoOperations: ReactiveMongoOperations): MongoMovieRepository {
        // TODO: this could be moved to more fitting place
        val indexOps = mongoOperations.indexOps(Movie::class.java)
        indexOps.ensureIndex(TextIndexDefinition.TextIndexDefinitionBuilder().onField("title").build()).block()
        indexOps.ensureIndex(Index("imdbId", Sort.Direction.ASC).background().unique()).block()
        return MongoMovieRepository(mongoOperations)
    }
}
