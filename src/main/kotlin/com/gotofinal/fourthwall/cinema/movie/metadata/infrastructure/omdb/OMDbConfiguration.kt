package com.gotofinal.fourthwall.cinema.movie.metadata.infrastructure.omdb

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient
import reactor.netty.resources.ConnectionProvider

@Configuration
@EnableConfigurationProperties(OMDbClientProperties::class)
internal class OMDbConfiguration {
    @Bean
    fun omdbHttpClient(
        properties: OMDbClientProperties
    ): OMDbHttpClient {
        val apiKey = properties.apiKey
        if (apiKey.isNullOrBlank()) {
            throw IllegalStateException("Missing api key to omdb. Provide via properties or system properties omdb.apiKey")
        }

        val httpClient: HttpClient = HttpClient.create(
            ConnectionProvider.builder("omdbHttpClient")
                .maxConnections(properties.maxConnections)
                .build()
        )
        val webClient: WebClient.Builder = WebClient.builder()
            .clientConnector(ReactorClientHttpConnector(httpClient))
        return OMDbHttpClient(
            uri = properties.uri,
            apiKey = apiKey,
            webClient = webClient.build(),
            timeout = properties.timeout
        )
    }
}
