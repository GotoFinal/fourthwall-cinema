package com.gotofinal.fourthwall.cinema.movie.infrastructure

import com.gotofinal.fourthwall.cinema.movie.Movie
import com.gotofinal.fourthwall.cinema.movie.MovieRepository
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import com.gotofinal.fourthwall.cinema.shared.paging.toPagedResult
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.Comparator
import java.util.concurrent.ConcurrentHashMap

internal class InMemoryMovieRepository : MovieRepository {
    private val storage: MutableMap<String, Movie> = ConcurrentHashMap()
    override fun getById(id: String): Mono<Movie> {
        return Mono.fromCallable { storage[id] }
    }

    override fun getByImdbId(imdbId: List<String>): Flux<Movie> {
        return Flux.fromIterable(storage.values.filter { it.toDto().imdbId in imdbId })
    }

    override fun save(movie: Movie): Mono<Movie> {
        return Mono.fromRunnable<Void> { storage[movie.id] = movie }
            .thenReturn(movie)
    }

    override fun delete(id: String): Mono<Void> {
        return Mono.fromRunnable { storage.remove(id) }
    }

    override fun search(request: GetMoviesRequest): Mono<SearchResult<MovieDto>> {
        return Flux.fromIterable(storage.values)
            .map { it.toDto() }
            .sort(Comparator.comparing { it.title })
            .filter { it.title.contains(request.phrase ?: "") }
            .collectList()
            .map { toPagedResult(it, request.pageable) }
    }
}
