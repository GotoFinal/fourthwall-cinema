package com.gotofinal.fourthwall.cinema.movie.metadata

import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import reactor.core.publisher.Mono

// TODO: maybe in memory client too, if there will be any logic inside it
interface MovieMetadataClient {
    fun fetchMetadata(movieDto: MovieDto): Mono<MovieWithMetadata>
}
