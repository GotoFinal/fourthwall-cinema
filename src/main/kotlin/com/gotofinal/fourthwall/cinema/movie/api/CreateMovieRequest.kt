package com.gotofinal.fourthwall.cinema.movie.api

class CreateMovieRequest(
    val title: String,
    val imdbId: String
)
