package com.gotofinal.fourthwall.cinema.movie.api

import org.springframework.data.domain.Pageable

data class GetMoviesRequest(
    val phrase: String?,
    val pageable: Pageable
)

data class GetMoviesRequestParams(
    val phrase: String?
)
