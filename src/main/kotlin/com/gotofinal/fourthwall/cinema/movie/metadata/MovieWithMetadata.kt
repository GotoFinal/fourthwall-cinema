package com.gotofinal.fourthwall.cinema.movie.metadata

import com.fasterxml.jackson.annotation.JsonAlias

data class MovieWithMetadata(
    val id: String,
    val title: String,
    val genre: String,
    val description: String,
    val image: String,
    val language: String,
    val rating: List<MovieRating>,
    val runtime: String,
    val release: String,
    val metadataProvider: String,
    val metadataId: String
)

data class MovieRating(
    @JsonAlias("Source")
    val source: String,
    @JsonAlias("Value")
    val value: String
)
