package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.movie.api.CreateMovieRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieMetadataClient
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieWithMetadata
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import com.gotofinal.fourthwall.cinema.shared.logger.logger
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.slf4j.Logger
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import javax.annotation.PostConstruct

// TODO: maybe update method, but i don't expect movie titles to change, and typos usually should be noticed before using movie in other places
class MovieFacade internal constructor(
    private val repository: MovieRepository,
    private val idGenerator: IdGenerator,
    private val properties: MovieConfigurationProperties,
    private val movieMetadataClient: MovieMetadataClient
) {
    private val logger: Logger by logger()

    fun getMovie(id: String): Mono<MovieDto> {
        return repository.getById(id).map { it.toDto() }
    }

    fun getMovieWithMetadata(id: String): Mono<MovieWithMetadata> {
        return getMovie(id)
            .flatMap { movieMetadataClient.fetchMetadata(it) }
    }

    fun search(request: GetMoviesRequest): Mono<SearchResult<MovieDto>> {
        return repository.search(request)
    }

    fun create(request: CreateMovieRequest): Mono<MovieDto> {
        // TODO: validate if imdbId of movie exists before save
        return repository.save(
            Movie(
                id = idGenerator.newId(),
                title = request.title,
                imdbId = request.imdbId
            )
        ).map { it.toDto() }
    }

    fun delete(id: String): Mono<Void> {
        return repository.delete(id)
    }

    fun feedMovies(movies: List<DefaultMovie>): Mono<Long> {
        val moviesMap = movies.map { it.imdb to it }.toMap()
        val ids = moviesMap.keys.toList()
        return repository.getByImdbId(ids)
            .map { it.toDto().imdbId }
            .collectList()
            .map { ids - it }
            .flatMapMany { Flux.fromIterable(it) }
            .flatMap { id ->
                create(CreateMovieRequest(moviesMap.getValue(id).title, moviesMap.getValue(id).imdb))
                    .doOnError { logger.error("Failed to add movie: ${moviesMap.getValue(id)}", it) }
                    .onErrorResume { Mono.empty() }
            }
            .count()
    }

    @PostConstruct
    fun feedDefaultMovies() {
        feedMovies(properties.movies)
            .subscribeOn(Schedulers.elastic())
            .subscribe()
    }
}
