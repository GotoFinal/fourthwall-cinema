package com.gotofinal.fourthwall.cinema.movie

import com.gotofinal.fourthwall.cinema.movie.metadata.MovieMetadataClient
import com.gotofinal.fourthwall.cinema.shared.id.IdGeneratorFactory
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableConfigurationProperties(MovieConfigurationProperties::class)
internal class MovieConfiguration {
    @Bean
    fun movieFacade(
        idGeneratorFactory: IdGeneratorFactory,
        movieRepository: MovieRepository,
        properties: MovieConfigurationProperties,
        movieMetadataClient: MovieMetadataClient
    ): MovieFacade {
        return MovieFacade(
            idGenerator = idGeneratorFactory.create("movie"),
            repository = movieRepository,
            properties = properties,
            movieMetadataClient = movieMetadataClient
        )
    }
}

@ConfigurationProperties("movies")
@ConstructorBinding
internal data class MovieConfigurationProperties(
    val movies: List<DefaultMovie> = listOf()
)

@ConstructorBinding
data class DefaultMovie(val title: String, val imdb: String)
