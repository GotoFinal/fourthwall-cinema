package com.gotofinal.fourthwall.cinema.movie.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoQueries.queryById
import com.gotofinal.fourthwall.cinema.infrastructure.mongo.unpaged
import com.gotofinal.fourthwall.cinema.movie.Movie
import com.gotofinal.fourthwall.cinema.movie.MovieRepository
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.Sort
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.core.query.TextCriteria.forDefaultLanguage
import org.springframework.data.mongodb.core.query.TextQuery.queryText
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

internal class MongoMovieRepository(
    private val mongo: ReactiveMongoOperations,
) : MovieRepository {
    override fun getById(id: String): Mono<Movie> {
        return mongo.findOne(queryById(id), Movie::class.java)
    }

    override fun getByImdbId(imdbId: List<String>): Flux<Movie> {
        return mongo.find(Query(Criteria("imdbId").`in`(imdbId)), Movie::class.java)
    }

    override fun save(movie: Movie): Mono<Movie> {
        return mongo.save(movie)
    }

    override fun delete(id: String): Mono<Void> {
        return mongo.remove(queryById(id), Movie::class.java).then()
    }

    override fun search(request: GetMoviesRequest): Mono<SearchResult<MovieDto>> {
        val query: Query = if (request.phrase != null) {
            queryText(forDefaultLanguage().matching(request.phrase)).sortByScore()
        } else Query()
        query.with(Sort.by("title"))
            .with(request.pageable)
        return Mono.zip(find(query), count(query))
            .map { SearchResult(it.t1, request.pageable, it.t2) }
    }

    private fun find(query: Query): Mono<List<MovieDto>> {
        return mongo.find(query, Movie::class.java)
            .map { it.toDto() }
            .collectList()
    }

    private fun count(query: Query): Mono<Long> {
        return mongo.count(query.unpaged(), Movie::class.java)
    }
}
