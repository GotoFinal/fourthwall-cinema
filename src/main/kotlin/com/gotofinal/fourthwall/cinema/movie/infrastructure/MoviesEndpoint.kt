package com.gotofinal.fourthwall.cinema.movie.infrastructure

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.movie.api.CreateMovieRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequest
import com.gotofinal.fourthwall.cinema.movie.api.GetMoviesRequestParams
import com.gotofinal.fourthwall.cinema.movie.api.MovieDto
import com.gotofinal.fourthwall.cinema.movie.metadata.MovieWithMetadata
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/movies")
internal class MoviesEndpoint(
    private val movieFacade: MovieFacade
) {
    @GetMapping(produces = [APPLICATION_JSON_VALUE])
    fun search(params: GetMoviesRequestParams, pageable: Pageable): Mono<SearchResult<MovieDto>> {
        return movieFacade.search(GetMoviesRequest(params.phrase, pageable))
    }

    @GetMapping(
        path = ["/{id}"],
        produces = [APPLICATION_JSON_VALUE]
    )
    fun getMovieWithMetadata(@PathVariable id: String): Mono<MovieWithMetadata> {
        return movieFacade.getMovieWithMetadata(id)
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun delete(@PathVariable id: String): Mono<Void> {
        return movieFacade.delete(id)
    }

    @PostMapping(
        produces = [APPLICATION_JSON_VALUE],
        consumes = [APPLICATION_JSON_VALUE]
    )
    @PreAuthorize("hasRole('ADMIN')")
    fun create(
        @RequestBody request: CreateMovieRequest
    ): Mono<MovieDto> {
        return movieFacade.create(request)
    }
}
