package com.gotofinal.fourthwall.cinema.movie.api

data class MovieDto(
    val id: String,
    val title: String,
    val imdbId: String,
)
