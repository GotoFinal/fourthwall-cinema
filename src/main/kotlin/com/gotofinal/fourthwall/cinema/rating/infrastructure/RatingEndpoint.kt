package com.gotofinal.fourthwall.cinema.rating.infrastructure

import com.gotofinal.fourthwall.cinema.rating.RatingFacade
import com.gotofinal.fourthwall.cinema.rating.api.CreateRatingRequest
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/ratings")
internal class RatingEndpoint(
    private val ratingFacade: RatingFacade
) {
    @GetMapping(
        path = ["/movie/{movieId}"],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun search(
        @PathVariable movieId: String,
        pageable: Pageable
    ): Mono<SearchResult<RatingDto>> {
        return ratingFacade.getLastReviews(movieId, pageable)
    }

    @PostMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    fun create(
        @RequestBody request: CreateRatingRequest
    ): Mono<RatingDto> {
        return ratingFacade.create(request)
    }
}
