package com.gotofinal.fourthwall.cinema.rating.infrastructure

import com.gotofinal.fourthwall.cinema.rating.Rating
import com.gotofinal.fourthwall.cinema.rating.RatingRepository
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import com.gotofinal.fourthwall.cinema.shared.paging.toPagedResult
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.Comparator
import java.util.concurrent.ConcurrentHashMap

internal class InMemoryRatingRepository : RatingRepository {
    private val storage: MutableMap<String, Rating> = ConcurrentHashMap()

    override fun save(rating: Rating): Mono<Rating> {
        return Mono.fromRunnable<Void> { storage[rating.id] = rating }
            .thenReturn(rating)
    }

    override fun getLastReviews(movieId: String, pageable: Pageable): Mono<SearchResult<RatingDto>> {
        return Flux.fromIterable(storage.values)
            .map { it.toDto() }
            .filter { it.movieId == movieId }
            .sort(Comparator.comparing { it.createdAt })
            .collectList()
            .map { toPagedResult(it, pageable) }
    }
}
