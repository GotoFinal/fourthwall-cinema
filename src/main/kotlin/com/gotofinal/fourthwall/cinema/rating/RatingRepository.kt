package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Mono

// TODO: some aggregation of reviews
internal interface RatingRepository {
    fun save(rating: Rating): Mono<Rating>
    fun getLastReviews(movieId: String, pageable: Pageable): Mono<SearchResult<RatingDto>>
}
