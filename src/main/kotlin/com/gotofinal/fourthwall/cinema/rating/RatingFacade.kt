package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.rating.api.CreateRatingRequest
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import com.gotofinal.fourthwall.cinema.shared.validation.ValidationException
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import java.time.Clock

class RatingFacade internal constructor(
    private val movieFacade: MovieFacade,
    private val repository: RatingRepository,
    private val idGenerator: IdGenerator,
    private val clock: Clock
) {
    // TODO: would be good to add some extra validation about user name and review
    fun create(request: CreateRatingRequest): Mono<RatingDto> {
        return validateRequest(request)
            .map {
                Rating(
                    id = idGenerator.newId(),
                    user = request.user,
                    createdAt = clock.instant(),
                    movieId = request.movieId,
                    rating = request.rating,
                    review = request.review
                )
            }
            .flatMap { repository.save(it) }
            .map { it.toDto() }
    }

    private fun validateRequest(request: CreateRatingRequest): Mono<CreateRatingRequest> {
        if (request.rating > MAX_RATING || request.rating < MIN_RATING) {
            return Mono.error(ValidationException("Rating must be between 1 and 5, but got: ${request.rating}"))
        }
        if (request.user.length > 200) {
            return Mono.error(ValidationException("User name is too long. > 200"))
        }
        if (request.review != null && request.review.length > 2000) {
            return Mono.error(ValidationException("Review is too long. > 2000"))
        }
        return movieFacade.getMovie(request.movieId)
            .switchIfEmpty { Mono.error(ValidationException("Can't rate not existing movie ${request.movieId}")) }
            .thenReturn(request)
    }

    fun getLastReviews(movieId: String, pageable: Pageable): Mono<SearchResult<RatingDto>> {
        return repository.getLastReviews(movieId, pageable)
    }

    // TODO: cinema owner should be able to remove reviews?

    companion object {
        const val MAX_RATING = 5
        const val MIN_RATING = 1
    }
}
