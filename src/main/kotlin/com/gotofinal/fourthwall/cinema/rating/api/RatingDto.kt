package com.gotofinal.fourthwall.cinema.rating.api

import java.time.Instant

data class RatingDto(
    val id: String,
    val user: String,
    val createdAt: Instant,
    val movieId: String,
    val rating: Int,
    val review: String?
)
