package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoCollections
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(MongoCollections.RATINGS)
internal class Rating(
    val id: String,
    private val user: String,
    private val createdAt: Instant,
    private val movieId: String,
    private val rating: Int,
    private val review: String?
) {
    fun toDto(): RatingDto {
        return RatingDto(
            id = id,
            user = user,
            createdAt = createdAt,
            movieId = movieId,
            rating = rating,
            review = review
        )
    }
}
