package com.gotofinal.fourthwall.cinema.rating.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.IN_MEM
import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.NOT_IN_MEM
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.ReactiveMongoOperations

@Configuration
internal class RatingRepositoryConfiguration {
    @Bean
    @Profile(IN_MEM)
    fun inMemoryRatingRepository(): InMemoryRatingRepository {
        return InMemoryRatingRepository()
    }

    @Bean
    @Profile(NOT_IN_MEM)
    fun mongoRatingRepository(mongoOperations: ReactiveMongoOperations): MongoRatingRepository {
        return MongoRatingRepository(mongoOperations)
    }
}
