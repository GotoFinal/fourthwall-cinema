package com.gotofinal.fourthwall.cinema.rating.api

data class CreateRatingRequest(
    val user: String,
    val movieId: String,
    val rating: Int,
    val review: String?
)
