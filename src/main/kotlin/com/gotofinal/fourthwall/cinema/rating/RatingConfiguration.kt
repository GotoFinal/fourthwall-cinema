package com.gotofinal.fourthwall.cinema.rating

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.shared.id.IdGeneratorFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
internal class RatingConfiguration {
    @Bean
    fun ratingFacade(
        movieFacade: MovieFacade,
        idGeneratorFactory: IdGeneratorFactory,
        ratingRepository: RatingRepository,
        clock: Clock
    ): RatingFacade {
        return RatingFacade(
            movieFacade = movieFacade,
            idGenerator = idGeneratorFactory.create("rating"),
            repository = ratingRepository,
            clock = clock
        )
    }
}
