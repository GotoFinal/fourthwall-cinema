package com.gotofinal.fourthwall.cinema.rating.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoQueries.queryBy
import com.gotofinal.fourthwall.cinema.infrastructure.mongo.unpaged
import com.gotofinal.fourthwall.cinema.rating.Rating
import com.gotofinal.fourthwall.cinema.rating.RatingRepository
import com.gotofinal.fourthwall.cinema.rating.api.RatingDto
import com.gotofinal.fourthwall.cinema.shared.paging.SearchResult
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Mono

internal class MongoRatingRepository(
    private val mongo: ReactiveMongoOperations,
) : RatingRepository {
    override fun save(rating: Rating): Mono<Rating> {
        return mongo.save(rating)
    }

    override fun getLastReviews(movieId: String, pageable: Pageable): Mono<SearchResult<RatingDto>> {
        val query: Query = queryBy("movieId", movieId).with(pageable)
        return Mono.zip(find(query), count(query))
            .map { SearchResult(it.t1, pageable, it.t2) }
    }

    private fun find(query: Query): Mono<List<RatingDto>> {
        return mongo.find(query, Rating::class.java)
            .map { it.toDto() }
            .collectList()
    }

    private fun count(query: Query): Mono<Long> {
        return mongo.count(query.unpaged(), Rating::class.java)
    }
}
