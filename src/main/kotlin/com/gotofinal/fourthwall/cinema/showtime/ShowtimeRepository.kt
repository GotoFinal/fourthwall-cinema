package com.gotofinal.fourthwall.cinema.showtime

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

internal interface ShowtimeRepository {
    fun save(showtime: Showtime): Mono<Showtime>
    fun delete(id: String): Mono<Void>
    fun findAll(movieId: String?, fromDate: Instant, toDate: Instant): Flux<Showtime>
}
