package com.gotofinal.fourthwall.cinema.showtime.api

import java.math.BigDecimal
import java.time.Instant

data class CreateShowtimeRequest(
    val startTime: Instant,
    val endTime: Instant,
    val movieId: String,
    val price: BigDecimal
)
