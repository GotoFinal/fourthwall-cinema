package com.gotofinal.fourthwall.cinema.showtime.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoQueries
import com.gotofinal.fourthwall.cinema.showtime.Showtime
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeRepository
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

internal class MongoShowtimeRepository(
    private val mongo: ReactiveMongoOperations,
) : ShowtimeRepository {
    override fun save(showtime: Showtime): Mono<Showtime> {
        return mongo.save(showtime)
    }

    override fun delete(id: String): Mono<Void> {
        return mongo.remove(MongoQueries.queryById(id), Showtime::class.java).then()
    }

    override fun findAll(movieId: String?, fromDate: Instant, toDate: Instant): Flux<Showtime> {
        val query: Query = Query.query(
            Criteria("startTime").gte(fromDate).lte(toDate)
        )
        if (movieId != null) {
            query.addCriteria(Criteria("movieId").`is`(movieId))
        }
        return mongo.find(query, Showtime::class.java)
    }
}
