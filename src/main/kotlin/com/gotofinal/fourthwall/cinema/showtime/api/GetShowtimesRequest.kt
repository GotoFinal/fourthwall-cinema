package com.gotofinal.fourthwall.cinema.showtime.api

import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate

data class GetShowtimesRequest(
    val movieId: String?,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val fromDate: LocalDate,
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val toDate: LocalDate
)
