package com.gotofinal.fourthwall.cinema.showtime

import com.gotofinal.fourthwall.cinema.infrastructure.mongo.MongoCollections
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import org.springframework.data.mongodb.core.mapping.Document
import java.math.BigDecimal
import java.time.Instant

@Document(MongoCollections.SHOWTIMES)
internal class Showtime(
    val id: String,
    val startTime: Instant,
    private val endTime: Instant,
    val movieId: String,
    private val imdbId: String,
    private val price: BigDecimal
) {
    fun toDto(): ShowtimeDto {
        return ShowtimeDto(
            id = id,
            startTime = startTime,
            endTime = endTime,
            movieId = movieId,
            imdbId = imdbId,
            price = price
        )
    }
}
