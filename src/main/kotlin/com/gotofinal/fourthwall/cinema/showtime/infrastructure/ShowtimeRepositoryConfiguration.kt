package com.gotofinal.fourthwall.cinema.showtime.infrastructure

import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.IN_MEM
import com.gotofinal.fourthwall.cinema.infrastructure.Profiles.NOT_IN_MEM
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.ReactiveMongoOperations

@Configuration
internal class ShowtimeRepositoryConfiguration {
    @Bean
    @Profile(IN_MEM)
    fun inMemoryShowtimeRepository(): InMemoryShowtimeRepository {
        return InMemoryShowtimeRepository()
    }

    @Bean
    @Profile(NOT_IN_MEM)
    fun mongoShowtimeRepository(mongoOperations: ReactiveMongoOperations): MongoShowtimeRepository {
        return MongoShowtimeRepository(mongoOperations)
    }
}
