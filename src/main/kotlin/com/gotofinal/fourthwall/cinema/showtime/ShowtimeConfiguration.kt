package com.gotofinal.fourthwall.cinema.showtime

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.shared.id.IdGeneratorFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
internal class ShowtimeConfiguration {
    @Bean
    fun showtimeFacade(
        movieFacade: MovieFacade,
        idGeneratorFactory: IdGeneratorFactory,
        showtimeRepository: ShowtimeRepository,
        clock: Clock
    ): ShowtimeFacade {
        return ShowtimeFacade(
            movieFacade = movieFacade,
            idGenerator = idGeneratorFactory.create("showtime"),
            repository = showtimeRepository,
            clock = clock
        )
    }
}
