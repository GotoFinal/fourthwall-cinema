package com.gotofinal.fourthwall.cinema.showtime.infrastructure

import com.gotofinal.fourthwall.cinema.showtime.ShowtimeFacade
import com.gotofinal.fourthwall.cinema.showtime.api.CreateShowtimeRequest
import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/showtimes")
internal class ShowtimeEndpoint(
    private val showtimeFacade: ShowtimeFacade
) {
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getShowtimes(
        getShowtimesRequest: GetShowtimesRequest
    ): Flux<ShowtimeDto> {
        return showtimeFacade.getShowtimes(getShowtimesRequest)
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    fun delete(@PathVariable id: String): Mono<Void> {
        return showtimeFacade.delete(id)
    }

    @PostMapping(
        produces = [MediaType.APPLICATION_JSON_VALUE],
        consumes = [MediaType.APPLICATION_JSON_VALUE]
    )
    @PreAuthorize("hasRole('ADMIN')")
    fun create(
        @RequestBody request: CreateShowtimeRequest
    ): Mono<ShowtimeDto> {
        return showtimeFacade.create(request)
    }
}
