package com.gotofinal.fourthwall.cinema.showtime.api

import java.math.BigDecimal
import java.time.Instant

data class ShowtimeDto(
    val id: String,
    val startTime: Instant,
    val endTime: Instant,
    val movieId: String,
    val imdbId: String,
    val price: BigDecimal
)
