package com.gotofinal.fourthwall.cinema.showtime

import com.gotofinal.fourthwall.cinema.movie.MovieFacade
import com.gotofinal.fourthwall.cinema.shared.id.IdGenerator
import com.gotofinal.fourthwall.cinema.shared.validation.ValidationException
import com.gotofinal.fourthwall.cinema.showtime.api.CreateShowtimeRequest
import com.gotofinal.fourthwall.cinema.showtime.api.GetShowtimesRequest
import com.gotofinal.fourthwall.cinema.showtime.api.ShowtimeDto
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import java.time.Clock
import java.time.LocalTime

class ShowtimeFacade internal constructor(
    private val movieFacade: MovieFacade,
    private val idGenerator: IdGenerator,
    private val repository: ShowtimeRepository,
    private val clock: Clock
) {
    fun create(request: CreateShowtimeRequest): Mono<ShowtimeDto> {
        return movieFacade.getMovie(request.movieId)
            .switchIfEmpty { Mono.error(ValidationException("Can't create showtime for not existing movie ${request.movieId}")) }
            .map {
                Showtime(
                    id = idGenerator.newId(),
                    startTime = request.startTime,
                    endTime = request.endTime,
                    imdbId = it.imdbId,
                    movieId = it.id,
                    price = request.price
                )
            }.flatMap { repository.save(it) }
            .map { it.toDto() }
    }

    fun delete(id: String): Mono<Void> {
        return repository.delete(id)
    }

    // TODO: sanity checks, to not try to get too much at once. + maybe if price is sane
    fun getShowtimes(request: GetShowtimesRequest): Flux<ShowtimeDto> {
        val fromDate = request.fromDate.atTime(LocalTime.MIN).atZone(clock.zone).toInstant()
        val toDate = request.toDate.atTime(LocalTime.MAX).atZone(clock.zone).toInstant()
        return repository.findAll(request.movieId, fromDate, toDate)
            .map { it.toDto() }
    }
}
