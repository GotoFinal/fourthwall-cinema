package com.gotofinal.fourthwall.cinema.showtime.infrastructure

import com.gotofinal.fourthwall.cinema.showtime.Showtime
import com.gotofinal.fourthwall.cinema.showtime.ShowtimeRepository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

internal class InMemoryShowtimeRepository : ShowtimeRepository {
    private val storage: MutableMap<String, Showtime> = ConcurrentHashMap()

    override fun save(showtime: Showtime): Mono<Showtime> {
        return Mono.fromRunnable<Void> { storage[showtime.id] = showtime }
            .thenReturn(showtime)
    }

    override fun delete(id: String): Mono<Void> {
        return Mono.fromRunnable { storage.remove(id) }
    }

    override fun findAll(movieId: String?, fromDate: Instant, toDate: Instant): Flux<Showtime> {
        return Flux.fromIterable(storage.values)
            .filter { movieId == null || it.movieId == movieId }
            .filter { (it.startTime.isAfter(fromDate) || it.startTime == fromDate) && (it.startTime.isBefore(toDate) || it.startTime == toDate) }
    }
}
