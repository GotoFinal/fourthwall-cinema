package com.gotofinal.fourthwall.cinema.shared.validation

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

class ValidationException(message: String) : ResponseStatusException(HttpStatus.BAD_REQUEST, message)
