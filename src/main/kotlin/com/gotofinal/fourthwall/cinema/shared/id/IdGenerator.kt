package com.gotofinal.fourthwall.cinema.shared.id

interface IdGenerator {
    fun newId(): String
}

interface IdGeneratorFactory {
    fun create(seed: String): IdGenerator
}
