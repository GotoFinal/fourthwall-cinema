package com.gotofinal.fourthwall.cinema.shared.paging

import org.springframework.data.domain.Pageable
import kotlin.math.ceil

data class SearchResult<T>(
    val content: List<T>,
    val page: Int,
    val pageSize: Int,
    val totalElements: Long,
    val size: Int = content.size,
    val totalPages: Int = ceil(totalElements / (pageSize.toDouble())).toInt()
) {
    constructor(content: List<T>, pageable: Pageable, totalElements: Long) :
        this(content, getPageNumber(pageable), getPageSize(content, pageable), totalElements)
}

private fun getPageNumber(pageable: Pageable): Int {
    if (pageable.isPaged) {
        return pageable.pageNumber
    }
    return 0
}

private fun getPageSize(content: List<*>, pageable: Pageable): Int {
    if (pageable.isPaged) {
        return pageable.pageSize
    }
    return content.size
}
