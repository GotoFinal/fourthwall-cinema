package com.gotofinal.fourthwall.cinema.shared.id

import org.hashids.Hashids
import java.time.Clock
import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.AtomicInteger

class HashIdGenerator internal constructor(
    private val clock: Clock,
    seed: String
) : IdGenerator {
    private val atomicInteger = AtomicInteger(ThreadLocalRandom.current().nextInt(1000))
    private val hashids: Hashids = Hashids(seed)

    override fun newId(): String {
        val milliseconds = clock.millis()
        val counter = atomicInteger.updateAndGet { i -> if (i > 4096) 0 else i + 1 }
        return hashids.encode(milliseconds, counter.toLong())
    }
}

class HashIdGeneratorFactory(private val clock: Clock) : IdGeneratorFactory {
    override fun create(seed: String): IdGenerator {
        return HashIdGenerator(clock, seed)
    }
}
