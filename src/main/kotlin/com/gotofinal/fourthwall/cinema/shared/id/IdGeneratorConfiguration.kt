package com.gotofinal.fourthwall.cinema.shared.id

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock

@Configuration
internal class IdGeneratorConfiguration {
    @Bean
    fun idGeneratorFactory(clock: Clock): IdGeneratorFactory {
        return HashIdGeneratorFactory(clock)
    }
}
