package com.gotofinal.fourthwall.cinema.shared.paging

import org.springframework.data.domain.Pageable

fun <T> toPagedResult(
    items: List<T>,
    pageable: Pageable,
    total: Long = items.size.toLong()
): SearchResult<T> {
    return SearchResult(applyPaging(items, pageable), pageable, total)
}

private fun <T> applyPaging(items: List<T>, pageable: Pageable): List<T> {
    return if (pageable.isPaged) {
        applyOffsetAndLimit(items, pageable.offset.toInt(), pageable.pageSize)
    } else {
        items
    }
}

private fun <T> applyOffsetAndLimit(items: List<T>, offset: Int, pageSize: Int): List<T> {
    return if (items.size < pageSize) {
        items
    } else {
        items.subList(offset, offset + pageSize)
    }
}
